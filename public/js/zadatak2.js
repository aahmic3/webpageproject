let assert = chai.assert;
describe('testoviparser', function() {
describe('dajTacnost()', function() {
it('tacnost 100%, lista gresaka prazna', function() {
    var objekatZaFun={
        "stats": {
        "suites": 2,
        "tests": 2,
        "passes": 2,
        "pending": 0,
        "failures": 0,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [],
        "passes": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }]}
        var test1=TestoviParser.dajTacnost(JSON.stringify(objekatZaFun));
        assert.equal(JSON.stringify(test1), '{"tacnost":"100%","greske":[]}');
});
it('tacnost 50%, lista gresaka ima jedan element', function() {
    var objekatZaFun={
        "stats": {
        "suites": 2,
        "tests": 2,
        "passes": 1,
        "pending": 0,
        "failures": 1,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [ 
            {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }
        ],
        "passes": [
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }]}
        var test2=TestoviParser.dajTacnost(JSON.stringify(objekatZaFun));
        assert.equal(JSON.stringify(test2), '{"tacnost":"50%","greske":["Tabela crtaj() should draw 3 rows when parameter are 2,3"]}');
});
it('Testovi se ne mogu izvršiti', function() {
    var objekatZaFun={
        "stats": {
        "suites": 2,
        "tests": 2,
        "passes": 2,
        "pending": 0,
        "failures": 0,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [],
        "passes": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }]}
        var test3=TestoviParser.dajTacnost(objekatZaFun);
        assert.equal(JSON.stringify(test3), '{"tacnost":"0%","greske":["Testovi se ne mogu izvršiti"]}');
});
it('Svi testovi padaju', function() {
    var objekatZaFun={
        "stats": {
        "suites": 2,
        "tests": 2,
        "passes": 0,
        "pending": 0,
        "failures": 0,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [
            { "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
        }
        ],
        "passes": []}
        var test4=TestoviParser.dajTacnost(JSON.stringify(objekatZaFun));
        assert.equal(JSON.stringify(test4), '{"tacnost":"0%","greske":["Tabela crtaj() should draw 3 rows when parameter are 2,3","Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3"]}');
});
it('tacnost 75%, tri testa prolaze, jedan pada', function() {
    var objekatZaFun={
        "stats": {
        "suites": 2,
        "tests": 4,
        "passes": 3,
        "pending": 0,
        "failures": 1,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 4 rows when parameter are 2,4",
        "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 2,4",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 3 columns in row 3 when parameter are 3,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [ 
        {
        "title": "should draw 3 columns in row 3 when parameter are 3,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "passes": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
            "title": "should draw 4 rows when parameter are 2,4",
            "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 2,4",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
        }
        ]}
        var test5=TestoviParser.dajTacnost(JSON.stringify(objekatZaFun));
        assert.equal(JSON.stringify(test5), '{"tacnost":"75%","greske":["Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3"]}');
});
it('rezultat 1 i rezultat 2 jednak, promjena 100%', function() {
    var objekatZaFun1={
        "stats": {
        "suites": 2,
        "tests": 2,
        "passes": 2,
        "pending": 0,
        "failures": 0,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [],
        "passes": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }]}
        var objekatZaFun2={
            "stats": {
            "suites": 2,
            "tests": 2,
            "passes": 2,
            "pending": 0,
            "failures": 0,
            "start": "2021-11-05T15:00:26.343Z",
            "end": "2021-11-05T15:00:26.352Z",
            "duration": 9
            },
            "tests": [
            {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }
            ],
            "pending": [],
            "failures": [],
            "passes": [
            {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }]}
        var test6=TestoviParser.porediRezultate(JSON.stringify(objekatZaFun1),JSON.stringify(objekatZaFun2));
        assert.equal(JSON.stringify(test6), '{"promjena":"100%","greske":[]}');
});
it('peomjena 50%, za rezultat1 i rezultat2 jednaki', function() {
    var objekatZaFun1={
        "stats": {
        "suites": 2,
        "tests": 2,
        "passes": 1,
        "pending": 0,
        "failures": 1,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [ 
            {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }
        ],
        "passes": [
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }]}
        var objekatZaFun2={
            "stats": {
            "suites": 2,
            "tests": 2,
            "passes": 1,
            "pending": 0,
            "failures": 1,
            "start": "2021-11-05T15:00:26.343Z",
            "end": "2021-11-05T15:00:26.352Z",
            "duration": 9
            },
            "tests": [
            {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }
            ],
            "pending": [],
            "failures": [ 
                {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
                }
            ],
            "passes": [
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }]}
        var test7=TestoviParser.porediRezultate(JSON.stringify(objekatZaFun1),JSON.stringify(objekatZaFun2));
        assert.equal(JSON.stringify(test7), '{"promjena":"50%","greske":["Tabela crtaj() should draw 3 rows when parameter are 2,3"]}');
});
it('promjena 50%, primjer sa foruma za pitanja', function() {
    var objekatZaFun1={
        "stats": {
        "suites": 2,
        "tests": 3,
        "passes": 1,
        "pending": 0,
        "failures": 2,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 4 rows when parameter are 2,4",
        "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 2,4",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }],
        "pending": [],
        "failures": [ 
            {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
                },
                {
                    "title": "should draw 4 rows when parameter are 2,4",
                    "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 2,4",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
        ],
        "passes": [
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }

        ]}
        var objekatZaFun2={
            "stats": {
            "suites": 2,
            "tests": 3,
            "passes": 2,
            "pending": 0,
            "failures": 1,
            "start": "2021-11-05T15:00:26.343Z",
            "end": "2021-11-05T15:00:26.352Z",
            "duration": 9
            },
            "tests": [
            {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 3 columns in row 3 when parameter are 3,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }
            ],
            "pending": [],
            "failures": [ 
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ],
            "passes": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                    },
                    {
                        "title": "should draw 3 columns in row 3 when parameter are 3,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
            ]}

            var test8=TestoviParser.porediRezultate(JSON.stringify(objekatZaFun1),JSON.stringify(objekatZaFun2));
            assert.equal(JSON.stringify(test8), '{"promjena":"50%","greske":["Tabela crtaj() should draw 4 rows when parameter are 2,4","Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3"]}');
});
it('promjena 50%, identični, dva padaju u rezultatu 2, provjera da li sortira', function() {
    var objekatZaFun1={
        "stats": {
        "suites": 2,
        "tests": 4,
        "passes": 3,
        "pending": 0,
        "failures": 1,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 4 rows when parameter are 2,4",
        "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 2,4",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 3 columns in row 3 when parameter are 3,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [ 
        {
        "title": "should draw 3 columns in row 3 when parameter are 3,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "passes": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
            "title": "should draw 4 rows when parameter are 2,4",
            "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 2,4",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
        }
        ]}
        var objekatZaFun2={
            "stats": {
            "suites": 2,
            "tests": 4,
            "passes": 2,
            "pending": 0,
            "failures": 2,
            "start": "2021-11-05T15:00:26.343Z",
            "end": "2021-11-05T15:00:26.352Z",
            "duration": 9
            },
            "tests": [
            {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 4 rows when parameter are 2,4",
            "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 2,4",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 3 columns in row 3 when parameter are 3,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }
            ],
            "pending": [],
            "failures": [ 
            {
                "title": "should draw 4 rows when parameter are 2,4",
                "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 2,4",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 3 columns in row 3 when parameter are 3,3",
                "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
                },
            ],
            "passes": [
            {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameterare 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            },
            ]}
            var test9=TestoviParser.porediRezultate(JSON.stringify(objekatZaFun1),JSON.stringify(objekatZaFun2));
            assert.equal(JSON.stringify(test9), '{"promjena":"50%","greske":["Tabela crtaj() should draw 2 columns in row 2 when parameterare 3,3","Tabela crtaj() should draw 4 rows when parameter are 2,4"]}');
});
});
});