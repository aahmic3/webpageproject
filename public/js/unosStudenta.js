function posalji(){
    var pom = {ime:"",prezime:"",index:"",grupa:""};
    pom.ime=(document.getElementById("ime").value).toString();
    pom.prezime=(document.getElementById("prezime").value).toString();
    pom.index=(document.getElementById("index").value).toString();
    pom.grupa=(document.getElementById("grupa").value).toString();
    Ajax.dodajStudenta(pom,function(e,data){
        if(e == null){
            document.getElementById("ajaxstatus").innerHTML = JSON.stringify(data);
        }
        else{
            document.getElementById("ajaxstatus").innerHTML = JSON.stringify(e);
        }
    });
}
function premjesti(){
    var index=(document.getElementById("index").value).toString();
    var grupa=(document.getElementById("grupa").value).toString();
    Ajax.postaviGrupu(index,grupa,function(e,data){
        if(e == null){
            document.getElementById("ajaxstatus").innerHTML = JSON.stringify(data);
        }
        else{
            document.getElementById("ajaxstatus").innerHTML = JSON.stringify(e);
        }
    });
}

function posaljiCSV(){
    var tekst=document.getElementById("podaci").value.toString();
    Ajax.dodajBatch(tekst,function(e,data){
        if(e == null){
            document.getElementById("ajaxstatus").innerHTML = JSON.stringify(data);
        }
        else{
            document.getElementById("ajaxstatus").innerHTML = JSON.stringify(e);
        }
    });
}