let TestoviParser  =(function(){
    var dajTacnost = function(parametar){
        try{
        var objekat=JSON.parse(parametar);
        var povratniObj={tacnost:"0%", greske:[]};
        var postotakTacnosti=objekat.stats.passes/objekat.stats.tests;
            postotakTacnosti*=100;
            povratniObj.tacnost=(Math.round(postotakTacnosti * 10) / 10).toString().concat("%");
            if(postotakTacnosti==100) return povratniObj;

            for(var i=0; i<objekat.failures.length; i++){
                povratniObj.greske.push(objekat.failures[i].fullTitle);
            }
            return povratniObj;
        }catch(error){
            return {tacnost:"0%", greske:["Testovi se ne mogu izvršiti"]};
        }
    }
    var imaju_u_rez1_ne_u_rez2=function(rezultat1, rezultat2){
        var broj=0;
        var padaju=null;
        var izlaz={br:0,padaju:[]}; 
        var nadjen=false;
        for(var i=0; i<rezultat1.failures.length; i++){
            for(var j=0; j<rezultat2.tests.length; j++){
            if(rezultat1.failures[i].title==rezultat2.tests[j].title){
                nadjen=true;
                break;
            }
        }
        if(!nadjen){
        broj++;
        izlaz.padaju.push(rezultat1.failures[i].fullTitle)
        }
        nadjen=false;
    }
    izlaz.padaju.sort();
    for(var i=0; i<rezultat2.failures.length; i++){
        for(var j=0; j<izlaz.padaju.length; j++){
            if(rezultat2.failures[i].title==izlaz.padaju[j].title){
                nadjen=true;
            }
        }
        if(!nadjen){
            izlaz.padaju.push(rezultat2.failures[i].fullTitle)
            }
            nadjen=false;
    }
        izlaz.br=broj;
        return izlaz;
    }
    var porediRezultate=function(rezultat1, rezultat2){
        try{
        var rez1=JSON.parse(rezultat1);
        var rez2=JSON.parse(rezultat2);
        var jednak=true;
        var izlaz={promjena:"0%",greske:[]};
        var petlja=0;
        if(rez2.tests.length>rez1.tests.length) petlja=rez1.tests.length;
        else if(rez1.tests.length>rez2.tests.length) petlja=rez2.tests.length;
        else petlja=rez2.tests.length;
        for(var i=0; i<petlja; i++){
        if(rez1.tests[i].title!=rez2.tests[i].title){
                    jednak=false;
                    break;
            }
        }
        if(jednak){ 
            izlaz.promjena=dajTacnost(JSON.stringify(rez2)).tacnost.toString();
            izlaz.greske=dajTacnost(JSON.stringify(rez2)).greske;
            izlaz.greske.sort();
        }else{
            izlaz.promjena=((imaju_u_rez1_ne_u_rez2(rez1,rez2).br+rez2.failures.length)/(imaju_u_rez1_ne_u_rez2(rez1,rez2).br+rez2.tests.length)*100).toString().concat("%");
            izlaz.greske=imaju_u_rez1_ne_u_rez2(rez1,rez2).padaju;
        }
        return izlaz;
        }catch(error){
        }
    }
    return{
        dajTacnost: dajTacnost,
        porediRezultate: porediRezultate
    }
    }());