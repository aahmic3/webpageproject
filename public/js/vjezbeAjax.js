let Ajax=(function(){
    var dodajInputPolja=function(DOMelementDIVauFormi,brojVjezbi){    
        // Clear previous contents of the container
        while (DOMelementDIVauFormi.hasChildNodes()) {
            DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
        }
        for (var i=0;i<brojVjezbi;i++){
            var labela = document.createElement("label");
            labela.innerHTML = "Broj zadataka za vježbu " + i +" ";
			DOMelementDIVauFormi.appendChild(labela);    
            // Create an <input> element, set its type and name attributes
            var input = document.createElement("input");
            input.type = "number";
            input.name = "z" + i;
            input.id = "z" + i;
            DOMelementDIVauFormi.appendChild(input);
            // Append a line break 
            DOMelementDIVauFormi.appendChild(document.createElement("br"));
        }
    }
        var posaljiPodatke = function(vjezbeObjekat,callbackFja){
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4 && ajax.status == 200)
                    callbackFja(null,JSON.parse(ajax.responseText));
                else if (ajax.readyState == 4){
                    callbackFja(ajax.status,null);
                }
            }
            ajax.open("POST", "http://localhost:3000/vjezbe", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(vjezbeObjekat));
        }    
    return{
            dodajInputPolja: dodajInputPolja,
            posaljiPodatke: posaljiPodatke}
}());