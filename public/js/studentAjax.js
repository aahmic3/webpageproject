let Ajax=(function(){
    var dodajStudenta = function(student,fnCallback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200)
                fnCallback(null,JSON.parse(ajax.responseText));
            else if (ajax.readyState == 4){
                fnCallback(ajax.status,null);
            }
        }
        ajax.open("POST", "http://localhost:3000/student", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(student));
    } 
    var postaviGrupu=function(index,grupa,fnCallback){
        var ajax = new XMLHttpRequest();
        var group={grupa:grupa};
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200)
                fnCallback(null,JSON.parse(ajax.responseText));
            else if (ajax.readyState == 4){
                fnCallback(ajax.status,null);
            }
        }
        ajax.open("PUT", "http://localhost:3000/student/"+index.toString(), true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(group));
    } 
    var dodajBatch=function(csvStudenti,fnCallback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200)
                fnCallback(null,JSON.parse(ajax.responseText));
            else if (ajax.readyState == 4){
                fnCallback(ajax.status,null);
            }
        }
        ajax.open("POST", "http://localhost:3000/batch/student", true);
        ajax.setRequestHeader("Content-Type", "text/plain");
        ajax.send(csvStudenti);
    }  
return{
        dodajStudenta: dodajStudenta,
        postaviGrupu:postaviGrupu,
        dodajBatch:dodajBatch
    }
}());