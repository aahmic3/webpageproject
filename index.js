const baza = require('./baza.js');
const express = require("express");
const bodyParser = require("body-parser");
const fs=require('fs');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.text())
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public/html'));
app.use('*/css',express.static('public/css'));
app.use('*/js',express.static('public/js'));
app.use('*/images',express.static('public/images'));

const {student}= require('./baza.js');
const {grupa}= require('./baza.js');
const {vjezba}= require('./baza.js');
const {zadatak}= require('./baza.js');

var brojZadatka=1;

app.get('/insert', function(req,res){
    let tijelo=req.body;
    grupa.create({
       naziv:'Grupa 1'
    }).catch((err) =>{
          console.log(err);
    });
  });
app.post('/student', async function(req,res){
  let tijelo=req.body;
  const grup =  await baza.grupa.findOne({ where: { naziv: tijelo.grupa } });
  if(grup==undefined){
     await grupa.create({
          naziv:tijelo.grupa
      })
  }
  const  s=await baza.student.findOne({where: { index: tijelo.index}});
  const group =  await baza.grupa.findOne({ where: { naziv: tijelo.grupa } });
  if(s!=undefined){
    res.send({status:"Student sa indexom "+tijelo.index.toString()+" već postoji!"});
    return;
  }
  student.create({
      ime: tijelo.ime,
      prezime: tijelo.prezime,
      index: tijelo.index,
      grupaId: group.id
  }).catch((err) =>{
        console.log(err);
  });
  res.send({status:"Kreiran student!"});
});

app.put('/student/:index', async function(req,res){
    let tijelo=req.body;
    const s=await baza.student.findOne({where: {index: req.params.index}});
    if(s==undefined){
        res.json({status:"Student sa indexom "+req.params.index.toString()+" ne postoji"});
        return;
    }
    const grup =  await baza.grupa.findOne({ where: { naziv: tijelo.grupa } });
    if(grup==undefined){
       await grupa.create({
            naziv:tijelo.grupa
        })
    }
    const g=await baza.grupa.findOne({ where: { naziv: tijelo.grupa } });
    s.grupaId=g.id;
    await s.save();
    res.status(200).json({
        status:"Promjenjena grupa studentu "+req.params.index.toString()
    });
})

app.post('/vjezbe',async function(req,res){
    let tijelo = req.body;
    var greškaBroja=false;

    if(tijelo.brojVjezbi<1 || tijelo.brojVjezbi>15 || tijelo.brojVjezbi!=tijelo.brojZadataka.length){
        greškaBroja=true;
    }
    var zadaciSaGreskom=[];
    var zadaci=[];
    for(var i=0; i<tijelo.brojZadataka.length; i++){
        if(tijelo.brojZadataka[i]<0 || tijelo.brojZadataka[i]>10){
            zadaciSaGreskom.push("z"+i);
        }
        zadaci.push(tijelo.brojZadataka[i]);
    }
    var pogresanParam="Pogresan parametar ";
        if(greškaBroja){
             data="";
             pogresanParam+="brojVjezbi";
        }
        if(zadaciSaGreskom.length!=0){
            pogresanParam+=","+zadaciSaGreskom;
        }
        if(pogresanParam=="Pogresan parametar "){
            baza.vjezba.destroy({
                where: {},
                truncate: false
              })
              baza.zadatak.destroy({
                where: {},
                truncate: false
              })
            for(var k=0; k<tijelo.brojVjezbi; k++){
            const novaVjezba=await vjezba.create({
                naziv:'Vjezba '+(k+1).toString(),
                brojZadataka:zadaci[k],
                brojVjezbe:(k+1).toString()
            }).catch((err) =>{
                  console.log(err);
            });
        }
        baza.vjezba.findAll().then((data)=>{
            //res.send(data);
            for(var k=0; k<data.length; k++){
                for(var j=0; j<data[k].brojZadataka; j++){
                     baza.zadatak.create({
                        naziv:'Zadatak'+(brojZadatka).toString(),
                        vjezbaId:data[k].id
                }).catch((err) =>{
                      console.log(err);
                });
                brojZadatka++;
                }
            }
            });
            res.send(tijelo);
        }
        else{
           res.send({status:"error",data:pogresanParam});
        }
 })

 app.get('/vjezbe', function (req, res) {
    baza.vjezba.findAll().
        then((data) => {
            var duz=data.length;
            var result={brojVjezbi:0,brojZadataka:[]};
            for(var i=0; i<duz; i++){
                result.brojZadataka.push(data[i].brojZadataka)
            }
            result.brojVjezbi=duz;
            res.send(result);
        })
        .catch((error) => {
            console.log(error);
        });
})

 baza.sequelize.sync().then((req) =>{
    app.listen(3000);
 });

app.post('/batch/student',async function(req,res){
    let tijelo = req.body;
    let tekst=tijelo.split('\n');
    let studenti=[];
    for(var i=0; i<tekst.length; i++){
        var noviStudent=tekst[i].split(',');
        studenti.push(noviStudent);
    }
    var nizObjekata=[];
    for(var i=0; i<studenti.length; i++){
        var objekatStudent={ime:studenti[i][0],prezime:studenti[i][1],index:studenti[i][2],grupa:studenti[i][3]};
        nizObjekata.push(objekatStudent);
    }
    var postojeci=[];
    var brojacPostojecih=0;
    var brojacNovih=0;
    for(var i=0; i<nizObjekata.length; i++){
        const grup =  await baza.grupa.findOne({ where: { naziv: nizObjekata[i].grupa } });
            if(grup==undefined){
            await grupa.create({
                naziv:nizObjekata[i].grupa
            }).catch((err) =>{
                console.log(err);
            });
            }
        const s=await baza.student.findOne({where: {index: nizObjekata[i].index}});
        const group =  await baza.grupa.findOne({ where: { naziv: nizObjekata[i].grupa } });
        if(s==undefined){
            brojacNovih++;
            student.create({
                ime: nizObjekata[i].ime,
                prezime: nizObjekata[i].prezime,
                index: nizObjekata[i].index,
                grupaId: group.id
            }).catch((err) =>{
                  console.log(err);
            });
        }
        else{
            brojacPostojecih++;
            postojeci.push(nizObjekata[i].index);
        }   
    }
    if(brojacPostojecih!=0){
       res.send( {status:"Dodano {"+brojacNovih+"} studenata, a studenti {"+postojeci.toString()+"} već postoje!"})
    }
    else{
        res.send( {status:"Dodano {"+brojacNovih+"} studenata!"});
    }

})








