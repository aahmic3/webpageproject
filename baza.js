const Sequelize = require("sequelize");
const grupa = require("./grupa.js");
const student = require("./student.js");
const sequelize = new Sequelize('wt18390', 'root', 'password', {
    host: 'localhost',
    dialect: 'mysql'
});
const baza = {}
baza.Sequelize = Sequelize;  
baza.sequelize = sequelize;
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
//import modela

baza.student = require('./student.js') (sequelize); 
baza.vjezba = require('./vjezba.js') (sequelize); 
baza.zadatak = require('./zadatak.js') (sequelize); 
baza.grupa= require('./grupa.js') (sequelize);
//relacije
//Vjezba 1-N zadataka
baza.vjezba.hasMany(baza.zadatak, { as: 'idZadatka' });

//Student 1-1 grupa
baza.grupa.hasOne(baza.student,{
    foreignKey:student
})
//Grupa 1-N vjezbi
//baza.grupa.hasMany(baza.vjezba, {as:'vjezbeGrupa', onDelete: 'cascade' , foreignKey: { allowNull: false } });

module.exports=baza